console.log("Hello World")

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section) */

console.log(" ")

function getSection(...numbers){
    let x = 1;
    let totalScore = 0;

    for(const number of numbers){
        totalScore += number;
        x++;
    }

    let average = Math.round(totalScore / (x - 1));

    if(average <= 80){
        console.log("Your score is " +  average + ". You will proceed to Grade 10 Ruby.");
    }else if(average >= 81 && average <= 120){
        console.log("Your score is " +  average + ". You will proceed to Grade 10 Opal.");

    }else if(average >= 121 && average <= 160){
        console.log("Your score is " +  average + ". You will proceed to Grade 10 Sapphire.");

    }else if(average >= 161 && average <= 200){
        console.log("Your score is " +  average + ". You will proceed to Grade 10 Diamond.");

    }else {
        console.log(average + " is not a valid score! The Entrance Exam is 200 items only. Please enter a valid score!");
    }
}

getSection(201);


// 2.)  Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.
console.log(" ")

function findLongestWord(word)
{
  let findWord = word.match(/\w[a-z]{0,}/gi);
  let result = findWord[0];

  for(let x = 1 ; x < findWord.length ; x++)
  {
    if(result.length < findWord[x].length)
    {
    result = findWord[x];
    } 
  }
  return result;
}
console.log("The longest word is: ")
console.log(findLongestWord('The Englishman Who Went Up a Hill But Came Down a Mountain.'));


// 3.) Write a JavaScript function to find the first not repeated character.
console.log(" ")

function findRepeatedChar(character) {
  let charRes = character.split('');
  let result = '';
  let char = 0;
 
  for (let x = 0; x < charRes.length; x++) {
    char = 0;
 
    for (let y = 0; y < charRes.length; y++) 
    {
      if (charRes[x] === charRes[y]) {
        char+= 1;
      }
    }
 
    if (char < 2) {
      result = charRes[x];
      break;
    }
  }
  return result;
}
console.log("The repeated character is: ")
console.log(findRepeatedChar('abacddbec'));