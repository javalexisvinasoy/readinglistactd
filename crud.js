//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/

let http = require('http')

let port = 8000

const server = http.createServer(function(request, response) {
  if(request.url == '/' && request.method == 'GET') { 
  response.writeHead(200, {'Content-Type': 'text/plain'})
  response.end('Welcome to Ordering System!')
  } else if (request.url == '/dashboard' && request.method == 'GET') { 
  response.writeHead(200, {'Content-Type': 'text/plain'})
  response.end("Welcome to your User's Dashboard!")
  } else if (request.url == '/products' && request.method == 'GET') { 
  response.writeHead(200, {'Content-Type': 'text/plain'})
  response.end('Here are the available products')
  } else if(request.url == '/addproduct' && request.method == 'POST') {
    let request_body = ''

    request.on('data', function(data) {
      request_body += data
    })
    request.on('end', function() {
      request_body = JSON.parse(request_body)

      let new_product = {
        "name": request_body.name,
        "description": request_body.description,
        "price": request_body.price,
        "stocks": request_body.stocks
      }

      courses.push(new_product)
      console.log(products)

      response.writeHead(200, {'Content-Type': 'application/json'})
      response.write(JSON.stringify(new_product))
      response.end()
    })

  } else if(request.url = "/updateproduct" && request.method == "PUT"){

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Update a product to our resources');

  } else if(request.url = "/archiveproduct" && request.method == "DELETE"){

          response.writeHead(200, {'Content-Type': 'text/plain'});
          response.end('Archive product to our resources');

  }

});

server.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);
